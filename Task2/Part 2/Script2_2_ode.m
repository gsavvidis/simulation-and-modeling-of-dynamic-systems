%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
% a=2
% b=1
% e=x-x_hat
% a_tilde=a_hat-a
% b_tilde=b_hat-b
% u=5*sin(3*t);
% n=0.15*sin(2*pi*20*t)
%% Parallili Domi
% e_dot=a*e+a_tilde*x_hat-b_tilde*u
% a_dot_hat=-e*x_hat
% b_dot_hat=e*u

%% Mikti Domi
% e_dot=-theta_m*e+a_tilde*x-b_tilde*u
% a_dot_hat=-e*x
% b_dot_hat=e*u
clear all;
%% Matrix creation
t=0;
a=2;
b=1;
e=zeros(length(t),1);
x=zeros(length(t),1);
x_hat=zeros(length(t),1);
x_hat_p=zeros(length(t),1);
a_hat = zeros(length(t),1);
b_hat = zeros(length(t),1);

%% Initializations
theta_m=1;
u=5*sin(3*t);
n=0.15*sin(2*pi*20*t);
a_hat(1)=1;
b_hat(1)=0;
e(1,1)=x(1,1)+n-x_hat(1,1);

% State consists of [x_error, a_hat, b_hat, x]
t_1=[0:0.0001:20];
y_0=[e(1,1) a_hat(1) b_hat(1) x(1,1)];
options=odeset('RelTol',10^(-10),'AbsTol',10^(-11));
%% System Mikti Domi
[t,y] = ode45(@(t,x) ode_state_2_2_m_9046(t,x,theta_m,a,b),t_1,y_0,options);

for i=1:length(y)
x_hat(i,:)=y(i,4)-y(i,1);
end

%% System Parallili Domi
[t_p,y_p] = ode45(@(t,x) ode_state_2_2_p_9046(t,x,a,b),t_1,y_0,options);

for i=1:length(y_p)
x_hat_p(i,:)=y_p(i,4)-y_p(i,1);
end

%% Plots Mikti Domi
% Plots are designed for 1920x1080p
figure('Position',[50 40 900 960]);
subplot(2,1,1);
hold on;
plot(t,y(:,4), 'LineWidth',2.0, 'Color','blue');
plot(t,x_hat, 'LineWidth',2.0, 'Color','magenta');
legend({['$x$'],['$\hat{x}$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$x$'], 'interpreter','latex', 'fontsize',17);
title('State', 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,1,2);
plot(t,y(:,1), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$x_{error}$'], 'interpreter','latex', 'fontsize',14);
title('State estimation error', 'interpreter','latex', 'fontsize',11);


figure('Position',[1000 40 900 960]);
subplot(2,1,1);
plot(t,y(:,2), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{a}$'], 'interpreter','latex', 'fontsize',14);
title('a estimation', 'interpreter','latex', 'fontsize',11);

subplot(2,1,2);
plot(t,y(:,3), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{b}$'], 'interpreter','latex', 'fontsize',14);
title('b estimation', 'interpreter','latex', 'fontsize',11);

%% Plots Parallili Domi
figure('Position',[50 40 900 960]);
subplot(2,1,1);
hold on;
plot(t_p,y_p(:,4), 'LineWidth',2.0, 'Color','blue');
plot(t_p,x_hat_p, 'LineWidth',2.0, 'Color','magenta');
legend({['$x$'],['$\hat{x}$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$x$'], 'interpreter','latex', 'fontsize',17);
title('State', 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,1,2);
plot(t_p,y_p(:,1), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$x_{error}$'], 'interpreter','latex', 'fontsize',14);
title('State estimation error', 'interpreter','latex', 'fontsize',11);


figure('Position',[1000 40 900 960]);
subplot(2,1,1);
plot(t_p,y_p(:,2), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{a}$'], 'interpreter','latex', 'fontsize',14);
title('a estimation', 'interpreter','latex', 'fontsize',11);

subplot(2,1,2);
plot(t_p,y_p(:,3), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{b}$'], 'interpreter','latex', 'fontsize',14);
title('b estimation', 'interpreter','latex', 'fontsize',11);
