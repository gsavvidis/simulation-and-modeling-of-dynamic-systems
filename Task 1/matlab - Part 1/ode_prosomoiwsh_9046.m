%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
%% ode function based on sytem equations
function [dy] = ode_prosomoiwsh_9046(y,u,m,b,k)
    dy=zeros(2,1);
    dy(1)=y(2);
    dy(2)=-b*y(2)/m-k*y(1)/m+u/m;
    if isnan(dy)
        return;
    end
end