%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
%% Parameters
m=15; %kg
b=0.2; %kg/s
k=2; %kg/s^2

%% Model Values
t_ode=0;
y_init=[0,0];
y_final=zeros(101,2); %Position & Velocity
t_final=zeros(101,1); % Time
u_ode_final=zeros(101,1); %u 
u_ode_final(1)=5*sin(2*t_ode)+10.5; %u initialization
% Ode runs 100 times with a 0.1 second timestep, combining 10 seconds
for i=1:100
    [t,y] = ode45(@(t,y) ode_prosomoiwsh_9046(y,u_ode_final(i),m,b,k),[t_ode t_ode+0.1],y_init);
    t_ode=t_ode+0.1;
    % Storing values cooresponding to the 0.1 second intervals
    t_final(i+1)=t(size(y,1));
    y_final(i+1,:)=y(size(y,1),:);
    
    % Setting up next ode function
    y_init=y_final(i+1,:);
    u_ode_final(i+1)=5*sin(2*t_ode)+10.5;
end

%% Phi values
Phi_1=tf([0 -1 0],[1 3 2]);
Phi_2=tf([0 0 -1],[1 3 2]);
Phi_3=tf([0 0 1],[1 3 2]);
[Phi_rs_1,Phi_1_t,]=lsim(Phi_1,y_final(:,1),t_final);
[Phi_rs_2,Phi_2_t,]=lsim(Phi_2,y_final(:,1),t_final);
[Phi_rs_3,Phi_3_t,]=lsim(Phi_3,u_ode_final(:,1),t_final);
Phi_rs=[Phi_rs_1, Phi_rs_2, Phi_rs_3]; %Phi matrix

%% Estimated parameters
theta_0=y_final(:,1)'*Phi_rs*inv(Phi_rs'*Phi_rs);
b_hat = m*(theta_0(1)+3);
k_hat = m*(theta_0(2)+2);
m_hat = m;
fprintf(['Estimated Parameters: b = ',num2str(b_hat), ', k = ',num2str(k_hat), ' and m = ',num2str(m_hat), '\n'])

%% Plots
figure();
plot(t_final,y_final(:,1))
title({'Position'}, 'interpreter','latex', 'fontsize',17);

figure();
plot(t_final,y_final(:,2))
title({'Velocity'}, 'interpreter','latex', 'fontsize',17);




