%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
format long g;
%% Question 1
%% Model Values
t=[0:0.00001:10]';
Vout = zeros (length(t),2);
V1 = zeros (length(t),1);
V2 = ones (length(t),1);
for i=1:length(t)
    Vout(i,:)=v(t(i));
    V1(i)=2*sin(t(i));
end
V_c = Vout(:,1);


%% Phi values
Phi_1=tf([0 -1 0],[1 200 10000]);
Phi_2=tf([0 0 -1],[1 200 10000]);
Phi_3=tf([0 1 0],[1 200 10000]);
Phi_4=tf([0 0 1],[1 200 10000]);
Phi_5=tf([0 1 0],[1 200 10000]);
Phi_6=tf([0 0 1],[1 200 10000]);
[Phi_rs_1,Phi_1_t,]=lsim(Phi_1,V_c,t);
[Phi_rs_2,Phi_2_t,]=lsim(Phi_2,V_c,t);
[Phi_rs_3,Phi_3_t,]=lsim(Phi_3,V1,t);
[Phi_rs_4,Phi_4_t,]=lsim(Phi_4,V1,t);
[Phi_rs_5,Phi_5_t,]=lsim(Phi_5,V2,t);
[Phi_rs_6,Phi_6_t,]=lsim(Phi_6,V2,t);
Phi_rs=[Phi_rs_1, Phi_rs_2, Phi_rs_3, Phi_rs_4, Phi_rs_5, Phi_rs_6]; %Phi matrix

%% Estimated parameters
theta_0=(V_c'*Phi_rs*inv(Phi_rs'*Phi_rs))';
RC_1=1/(theta_0(1)+200);
RC_2=1/theta_0(3);
RC_3=1/theta_0(5);
LC_1=1/(theta_0(2)+10000);
LC_2=1/theta_0(6);

fprintf(['Estimated Parameters: RC = ',num2str(RC_1), ', ',num2str(RC_2), ' and ',num2str(RC_3), ', LC = ',num2str(LC_1), ' and ',num2str(LC_2), '\n'])

%% Plots
figure();
plot(t,Vout(:,1))
title({'$V_c$'}, 'interpreter','latex', 'fontsize',17);

figure();
plot(t,Vout(:,2))
title({['$V_R$']}, 'interpreter','latex', 'fontsize',17);

figure();
plot(t,V1)
title({['$V_1$']}, 'interpreter','latex', 'fontsize',17);

%% Question 2
%% Random Spikes

Vout_spi = zeros (length(t),2);
Vout_spi = Vout;
V_c_spi = Vout_spi(:,1);
for i=1:3
    Vout_spi(300000*i,1)=Vout_spi(300000*i,1)+10*randn()*Vout_spi(300000*i,1);
    Vout_spi(300000*i,2)=Vout_spi(300000*i,2)+10*randn()*Vout_spi(300000*i,2);
    V_c_spi(300000*i)=Vout_spi(300000*i,1);
end

%% Phi values
Phi_1_spi=tf([0 -1 0],[1 200 10000]);
Phi_2_spi=tf([0 0 -1],[1 200 10000]);
Phi_3_spi=tf([0 1 0],[1 200 10000]);
Phi_4_spi=tf([0 0 1],[1 200 10000]);
Phi_5_spi=tf([0 1 0],[1 200 10000]);
Phi_6_spi=tf([0 0 1],[1 200 10000]);
[Phi_rs_1_spi,Phi_1_t_spi,]=lsim(Phi_1_spi,V_c_spi,t);
[Phi_rs_2_spi,Phi_2_t_spi,]=lsim(Phi_2_spi,V_c_spi,t);
[Phi_rs_3_spi,Phi_3_t_spi,]=lsim(Phi_3_spi,V1,t);
[Phi_rs_4_spi,Phi_4_t_spi,]=lsim(Phi_4_spi,V1,t);
[Phi_rs_5_spi,Phi_5_t_spi,]=lsim(Phi_5_spi,V2,t);
[Phi_rs_6_spi,Phi_6_t_spi,]=lsim(Phi_6_spi,V2,t);
Phi_rs_spi=[Phi_rs_1_spi, Phi_rs_2_spi, Phi_rs_3_spi, Phi_rs_4_spi, Phi_rs_5_spi, Phi_rs_6_spi]; %Phi matrix

%% Estimated parameters
theta_0_spi=(V_c_spi'*Phi_rs_spi*inv(Phi_rs_spi'*Phi_rs_spi))';
RC_1_spi=1/(theta_0_spi(1)+200);
RC_2_spi=1/theta_0_spi(3);
RC_3_spi=1/theta_0_spi(5);
LC_1_spi=1/(theta_0_spi(2)+10000);
LC_2_spi=1/theta_0_spi(6);

fprintf(['Estimated Parameters with Spikes: RC = ',num2str(RC_1_spi), ', ',num2str(RC_2_spi), ' and ',num2str(RC_3_spi), ', LC = ',num2str(LC_1_spi), ' and ',num2str(LC_2_spi), '\n'])


%% Plots
figure();
plot(t,Vout_spi(:,1))
title({'$V_c \quad with \quad spikes$'}, 'interpreter','latex', 'fontsize',17);

figure();
plot(t,Vout_spi(:,2))
title({['$V_R \quad with \quad spikes$']}, 'interpreter','latex', 'fontsize',17);
