%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
%% ode function based on estimated system equations
function [dx] = ode_prosomoiwsh_9046_project_rt(t,x,a_m,gamma,y)
    u=sin(t)+0.1*cos(2*t);
    t
    y_current=y(t*1000+1,1); %The real system value
    y_current=1;
    x_hat=[x(3) x(4)]*[x(1) x(2)]';
    
    e=y_current-x_hat;
    
    dx(1,1)=-a_m*x(2)+y_current;
    dx(2,1)=-a_m*x(3)+u;
    
    dx(3,1)=gamma*e*x(2);
    dx(4,1)=gamma*e*x(3);
    if isnan(dx)
        return;
    end
end