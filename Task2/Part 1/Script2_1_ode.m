%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
% x_dot = -a*x+b*u;
% u=5*sin(3*t);
% x=[a_m-a; b]'[x/(s+a_m);u/(s+a_m)]; 
% theta=[a_m-a; b]';
% phi=[x/(s+a_m);u/(s+a_m)];
% phi=[x*e^(-a_m*t);u*e^(-a_m*t)];

% e=x-x_hat;
% theta_dot_hat_1=-gamma*e*phi_1;
% theta_dot_hat_2=-gamma*e*phi_2;
% phi_dot_1=-a_m*phi_1+x;
% phi_dot_2=-a_m*phi_2+u;

%% Matrix creation
t=0;
x=zeros(length(t),1);
phi=zeros(1,2);
theta_hat=zeros(1,2);
a_hat = zeros(length(t),1);
b_hat = zeros(length(t),1);

%% Initializations
a_m=1;
gamma=1;
u=5*sin(3*t(1));
phi(1,1)=x(1,1)*exp((-a_m*t(1)));
phi(1,2)=u*exp((-a_m*t(1)));
a_hat(1)=1;
b_hat(1)=0;
theta_hat(1,1)=a_m-a_hat(1);
theta_hat(1,2)=b_hat(1);

% State consists of [x, phi_1, phi_2, theta_hat_1, theta_hat_2]
t_1=[0:0.001:100];
y_0=[x(1,1) phi(1,1) phi(1,2) theta_hat(1,1) theta_hat(1,2)];
options=odeset('RelTol',10^(-10),'AbsTol',10^(-11));
%% System
[t,y] = ode45(@(t,x) ode_state_2_1_9046(t,x,a_m,gamma),t_1,y_0,options);
for i=1:length(y)
x_hat(i,:)=[y(i,4) y(i,5)]*[y(i,2) y(i,3)]';
end

x_err = y(:,1)-x_hat;
a_hat=a_m-y(:,4);
b_hat=y(:,5);

%% Plots
% Plots are designed for 1920x1080p
figure('Position',[50 40 900 960]);
subplot(2,1,1);
hold on;
plot(t,y(:,1), 'LineWidth',2.0, 'Color','blue');
plot(t,x_hat, 'LineWidth',2.0, 'Color','magenta');
legend({['$x$'],['$\hat{x}$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$x$'], 'interpreter','latex', 'fontsize',17);
title('State', 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,1,2);
plot(t,x_err, 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$x_{error}$'], 'interpreter','latex', 'fontsize',14);
title('State estimation error', 'interpreter','latex', 'fontsize',11);


figure('Position',[1000 40 900 960]);
subplot(2,1,1);
plot(t,a_hat, 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{a}$'], 'interpreter','latex', 'fontsize',14);
title('a estimation', 'interpreter','latex', 'fontsize',11);

subplot(2,1,2);
plot(t,b_hat, 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{b}$'], 'interpreter','latex', 'fontsize',14);
title('b estimation', 'interpreter','latex', 'fontsize',11);
