%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
t=[0:0.001:20]';
u=sin(t)+0.1*cos(2*t);
y=out(t,u);
%% Linear == Task 1
x=zeros(3,length(t));
x(1,:)=1;
x(2,:)=sin(t);
x(3,:)=sin(1000*t);

%% Law of Additivity with regards to input
k1=2;
k2=3;
k3=4;
y_additivity=zeros(6,length(t));
y_additivity(1,:)=out(t,x(1,:));
y_additivity(2,:)=out(t,x(2,:));
y_additivity(3,:)=out(t,x(3,:));
temp=y_additivity(1,:);
y_additivity(1,:)=k1*y_additivity(1,:)+k2*y_additivity(2,:);
y_additivity(2,:)=k2*y_additivity(2,:)+k3*y_additivity(3,:);
y_additivity(3,:)=k3*y_additivity(3,:)+k1*temp;
y_additivity(4,:)=out(t,k1*x(1,:)+k2*x(2,:));
y_additivity(5,:)=out(t,k2*x(2,:)+k3*x(3,:));
y_additivity(6,:)=out(t,k3*x(3,:)+k1*x(1,:));
%This is correct as shown in the plots

%% Law of Homogeneity
k4=3;
y_homogeneity=zeros(2,length(t));
y_homogeneity(1,:)=k4*y;
u_homogeinity=k4*u;
y_homogeneity(2,:)=out(t,u_homogeinity);

%% Linearity Calculation
error_threshold = 1e-6;
y_additivity_error(1,:)=(y_additivity(1,:)-y_additivity(4,:));
y_additivity_error(2,:)=(y_additivity(2,:)-y_additivity(5,:));
y_additivity_error(3,:)=(y_additivity(3,:)-y_additivity(6,:));
y_homogeneity_error(1,:)=(y_homogeneity(1,:)-y_homogeneity(2,:));
for i=1:length(t)
    if(y_additivity_error(1,i)>error_threshold) || (y_additivity_error(2,i)>error_threshold)||...
            (y_additivity_error(3,i)>error_threshold)||(y_homogeneity_error(1,i)>error_threshold)
        fprintf(['System is non-linear\n']);
        break;
    end
    if(i==length(t))
        fprintf(['System is linear\n']);
    end
end

%% Plots
figure();
subplot(2,1,1)
hold on;
plot(t,y_additivity(1,:))
plot(t,y_additivity(4,:))
legend({['$addition1$'],['$addition2$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$Y_{12}$'], 'interpreter','latex', 'fontsize',17);
title({'$Law of Additivity$'}, 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,1,2)
plot(t,y_additivity(1,:)-y_additivity(4,:))
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$Y_{12error}$'], 'interpreter','latex', 'fontsize',17);

figure();
subplot(2,1,1)
hold on;
plot(t,y_additivity(2,:))
plot(t,y_additivity(5,:))
legend({['$addition1$'],['$addition2$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$Y_{23}$'], 'interpreter','latex', 'fontsize',17);
title({'$Law of Additivity$'}, 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,1,2)
plot(t,y_additivity(2,:)-y_additivity(5,:))
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$Y_{23error}$'], 'interpreter','latex', 'fontsize',17);

figure();
subplot(2,1,1)
hold on;
plot(t,y_additivity(3,:))
plot(t,y_additivity(6,:))
legend({['$addition1$'],['$addition2$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$Y_{31}$'], 'interpreter','latex', 'fontsize',17);
title({'$Law of Additivity$'}, 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,1,2)
plot(t,y_additivity(3,:)-y_additivity(6,:))
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$Y_{31error}$'], 'interpreter','latex', 'fontsize',17);

figure();
subplot(2,1,1)
hold on;
plot(t,y_homogeneity(1,:))
plot(t,y_homogeneity(2,:))
legend({['$homogeneity1$'],['$homogeneity2$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$Y$'], 'interpreter','latex', 'fontsize',17);
title({'$Law of Homogeneity$'}, 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,1,2)
plot(t,y_homogeneity(1,:)-y_homogeneity(2,:))
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$Y_{error}$'], 'interpreter','latex', 'fontsize',17);

%% Estimation == Task 2
%% Non-Real time
%% [n,m]=[4,2]
%% Phi values
Phi_1=tf([0 -1 0 0 0],[1 3.4 15 15 12]);
Phi_2=tf([0 0 -1 0 0],[1 3.4 15 15 12]);
Phi_3=tf([0 0 0 -1 0],[1 3.4 15 15 12]);
Phi_4=tf([0 0 0 0 -1],[1 3.4 15 15 12]);
Phi_5=tf([0 1 0],[1 3.4 15 15 12]);
Phi_6=tf([0 0 1],[1 3.4 15 15 12]);
[Phi_rs_1,Phi_1_t,]=lsim(Phi_1,y(:,1),t);
[Phi_rs_2,Phi_2_t,]=lsim(Phi_2,y(:,1),t);
[Phi_rs_3,Phi_3_t,]=lsim(Phi_3,y(:,1),t);
[Phi_rs_4,Phi_4_t,]=lsim(Phi_4,y(:,1),t);
[Phi_rs_5,Phi_5_t,]=lsim(Phi_5,u(:,1),t);
[Phi_rs_6,Phi_6_t,]=lsim(Phi_6,u(:,1),t);
Phi_rs=[Phi_rs_1, Phi_rs_2, Phi_rs_3,Phi_rs_4,Phi_rs_5,Phi_rs_6]; %Phi matrix

%% Estimated Parameters
theta_0=y(:,1)'*Phi_rs*inv(Phi_rs'*Phi_rs);
a_hat=zeros(4,1);
b_hat=zeros(4,1);
a_hat(1,1) = theta_0(1)+3.4;
a_hat(2,1) = theta_0(2)+15;
a_hat(3,1) = theta_0(3)+15;
a_hat(4,1) = theta_0(4)+12;
b_hat(1,1) = theta_0(5);
b_hat(2,1) = theta_0(6);

fprintf(['Estimated Parameters 4th Der. - Offline: a1_hat = ',num2str(a_hat(1,1)), ', a2_hat = ',num2str(a_hat(2,1)), ...
    ', a3_hat = ',num2str(a_hat(3,1)),', a4_hat = ',num2str(a_hat(4,1)), ...
    ', b1_hat = ',num2str(b_hat(1,1)), ', b2_hat = ',num2str(b_hat(2,1)), '\n'])

%% Estimated model
y_0=[0,0,0,0];
options=odeset('RelTol',10^(-10),'AbsTol',10^(-11));
[t_ode,y_ode] = ode45(@(t,x) ode_prosomoiwsh_9046_project_four(t,x,a_hat,b_hat),t,y_0,options);
y_error=y(:,1)-y_ode(:,1);

%% Evaluation Criteria
N=length(y);
n=length(theta_0);
I=0;
for i=1:length(y)
    I=I+y_error(i)^2;
end
I=I/N;

r=2;
Akaike_Criteria=N*log(I)+r*n;
Bayes_Criteria=N*log(I)+log(N)*n;
Khinchin_Law_Criteria=N*log(I)+2*r*log(log(N))*n;
FPE_Criteria=N*log(I)+N*log((N+n)/(N-n));
r1=2;
r2=3;
if(((n*log(2*N)-log(n)+r2)/N)>0)
    SRM_Criteria=I/(r1*sqrt((n*log(2*N)-log(n)+r2)/N));
else
    fprintf(['Passed the model maximum complexity\n']);
end

fprintf(['Evaluation Criteria: Akaike = ',num2str(Akaike_Criteria),...
    ', Bayes = ',num2str(Bayes_Criteria), ...
    ', Khinchins Law = ',num2str(Khinchin_Law_Criteria),...
    ', FPE = ',num2str(FPE_Criteria), ...
    ', SRM = ',num2str(SRM_Criteria), '\n\n'])

%% Plots
figure();
subplot(2,1,1);
hold on;
plot(t,y)
plot(t_ode,y_ode(:,1))
legend({['$y$'],['$\hat{y}$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y$'], 'interpreter','latex', 'fontsize',17);
title('State 4th Der. - Offline', 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,1,2);
hold on;
plot(t,y_error)
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y_{error}$'], 'interpreter','latex', 'fontsize',17);
title('State Estimation Error', 'interpreter','latex', 'fontsize',17);
hold off;

%% [n,m]=[6,2]
%% Phi values
Phi_1=tf([0 -1 0 0 0 0 0],[1 3 6 9 22 22 15]);
Phi_2=tf([0 0 -1 0 0 0 0],[1 3 6 9 22 22 15]);
Phi_3=tf([0 0 0 -1 0 0 0],[1 3 6 9 22 22 15]);
Phi_4=tf([0 0 0 0 -1 0 0],[1 3 6 9 22 22 15]);
Phi_5=tf([0 0 0 0 0 -1 0],[1 3 6 9 22 22 15]);
Phi_6=tf([0 0 0 0 0 0 -1],[1 3 6 9 22 22 15]);
Phi_7=tf([0 1 0],[1 3 6 9 22 22 15]);
Phi_8=tf([0 0 1],[1 3 6 9 22 22 15]);
[Phi_rs_1,Phi_1_t,]=lsim(Phi_1,y(:,1),t);
[Phi_rs_2,Phi_2_t,]=lsim(Phi_2,y(:,1),t);
[Phi_rs_3,Phi_3_t,]=lsim(Phi_3,y(:,1),t);
[Phi_rs_4,Phi_4_t,]=lsim(Phi_4,y(:,1),t);
[Phi_rs_5,Phi_5_t,]=lsim(Phi_5,y(:,1),t);
[Phi_rs_6,Phi_6_t,]=lsim(Phi_6,y(:,1),t);
[Phi_rs_7,Phi_7_t,]=lsim(Phi_7,u(:,1),t);
[Phi_rs_8,Phi_8_t,]=lsim(Phi_8,u(:,1),t);
Phi_rs=[Phi_rs_1, Phi_rs_2, Phi_rs_3,Phi_rs_4,Phi_rs_5,Phi_rs_6,Phi_rs_7,Phi_rs_8]; %Phi matrix

%% Estimated Parameters
theta_0=y(:,1)'*Phi_rs*inv(Phi_rs'*Phi_rs);
a_hat=zeros(6,1);
b_hat=zeros(2,1);
a_hat(1,1) = theta_0(1)+3;
a_hat(2,1) = theta_0(2)+6;
a_hat(3,1) = theta_0(3)+9;
a_hat(4,1) = theta_0(4)+22;
a_hat(5,1) = theta_0(5)+22;
a_hat(6,1) = theta_0(6)+15;
b_hat(1,1) = theta_0(7);
b_hat(2,1) = theta_0(8);

fprintf(['Estimated Parameters 6th Der. - Offline: a1_hat = ',num2str(a_hat(1,1)), ', a2_hat = ',num2str(a_hat(2,1)), ...
    ', a3_hat = ',num2str(a_hat(3,1)),', a4_hat = ',num2str(a_hat(4,1)), ...
    ', a5_hat = ',num2str(a_hat(5,1)),', a6_hat = ',num2str(a_hat(6,1)), ...
    ', b1_hat = ',num2str(b_hat(1,1)), ', b2_hat = ',num2str(b_hat(2,1)), '\n'])

%% Estimated model
y_0=[0,0,0,0,0,0];
options=odeset('RelTol',10^(-10),'AbsTol',10^(-11));
[t_ode,y_ode] = ode45(@(t,x) ode_prosomoiwsh_9046_project_six(t,x,a_hat,b_hat),t,y_0,options);
y_error=y(:,1)-y_ode(:,1);

%% Evaluation Criteria
N=length(y);
n=length(theta_0);
I=0;
for i=1:length(y)
    I=I+y_error(i)^2;
end
I=I/N;

r=2;
Akaike_Criteria=N*log(I)+r*n;
Bayes_Criteria=N*log(I)+log(N)*n;
Khinchin_Law_Criteria=N*log(I)+2*r*log(log(N))*n;
FPE_Criteria=N*log(I)+N*log((N+n)/(N-n));
r1=2;
r2=3;
if(((n*log(2*N)-log(n)+r2)/N)>0)
    SRM_Criteria=I/(r1*sqrt((n*log(2*N)-log(n)+r2)/N));
else
    fprintf(['Passed the model maximum complexity\n']);
end

fprintf(['Evaluation Criteria: Akaike = ',num2str(Akaike_Criteria),...
    ', Bayes = ',num2str(Bayes_Criteria), ...
    ', Khinchins Law = ',num2str(Khinchin_Law_Criteria),...
    ', FPE = ',num2str(FPE_Criteria), ...
    ', SRM = ',num2str(SRM_Criteria), '\n\n'])

%% Plots
figure();
subplot(2,1,1);
hold on;
plot(t,y)
plot(t_ode,y_ode(:,1))
legend({['$y$'],['$\hat{y}$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y$'], 'interpreter','latex', 'fontsize',17);
title('State 6th Der. - Offline', 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,1,2);
hold on;
plot(t,y(:,1)-y_ode(:,1))
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y_{error}$'], 'interpreter','latex', 'fontsize',17);
title('State Estimation Error', 'interpreter','latex', 'fontsize',17);
hold off;

%% [n,m]=[2,2]
%% Phi values
Phi_1=tf([0 -1 0],[1 3 2]);
Phi_2=tf([0 0 -1],[1 3 2]);
Phi_3=tf([0 1 0],[1 3 2]);
Phi_4=tf([0 0 1],[1 3 2]);
[Phi_rs_1,Phi_1_t,]=lsim(Phi_1,y(:,1),t);
[Phi_rs_2,Phi_2_t,]=lsim(Phi_2,y(:,1),t);
[Phi_rs_3,Phi_3_t,]=lsim(Phi_3,u(:,1),t);
[Phi_rs_4,Phi_4_t,]=lsim(Phi_4,u(:,1),t);
Phi_rs=[Phi_rs_1, Phi_rs_2, Phi_rs_3,Phi_rs_4]; %Phi matrix

%% Estimated Parameters
theta_0=y(:,1)'*Phi_rs*inv(Phi_rs'*Phi_rs);
a_hat=zeros(2,1);
b_hat=zeros(2,1);
a_hat(1,1) = theta_0(1)+2;
a_hat(2,1) = theta_0(2)+3;
b_hat(1,1) = theta_0(3);
b_hat(2,1) = theta_0(4);

fprintf(['Estimated Parameters 2rd Der. - Offline: a1_hat = ',num2str(a_hat(1,1)), ', a2_hat = ',num2str(a_hat(2,1)), ...
    ', b1_hat = ',num2str(b_hat(1,1)), ', b2_hat = ',num2str(b_hat(2,1)), '\n'])

%% Estimated model
y_0=[0,0];
options=odeset('RelTol',10^(-10),'AbsTol',10^(-11));
[t_ode,y_ode] = ode45(@(t,x) ode_prosomoiwsh_9046_project_two(t,x,a_hat,b_hat),t,y_0,options);
y_error=y(:,1)-y_ode(:,1);

%% Evaluation Criteria
N=length(y);
n=length(theta_0);
I=0;
for i=1:length(y)
    I=I+y_error(i)^2;
end
I=I/N;

r=2;
Akaike_Criteria=N*log(I)+r*n;
Bayes_Criteria=N*log(I)+log(N)*n;
Khinchin_Law_Criteria=N*log(I)+2*r*log(log(N))*n;
FPE_Criteria=N*log(I)+N*log((N+n)/(N-n));
r1=2;
r2=3;
if(((n*log(2*N)-log(n)+r2)/N)>0)
    SRM_Criteria=I/(r1*sqrt((n*log(2*N)-log(n)+r2)/N));
else
    fprintf(['Passed the model maximum complexity\n']);
end

fprintf(['Evaluation Criteria: Akaike = ',num2str(Akaike_Criteria),...
    ', Bayes = ',num2str(Bayes_Criteria), ...
    ', Khinchins Law = ',num2str(Khinchin_Law_Criteria),...
    ', FPE = ',num2str(FPE_Criteria), ...
    ', SRM = ',num2str(SRM_Criteria), '\n\n'])

%% Plots
figure();
subplot(2,1,1);
hold on;
plot(t,y)
plot(t_ode,y_ode(:,1))
legend({['$y$'],['$\hat{y}$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y$'], 'interpreter','latex', 'fontsize',17);
title('State 2rd Der. - Offline', 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,1,2);
hold on;
plot(t,y(:,1)-y_ode(:,1))
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y_{error}$'], 'interpreter','latex', 'fontsize',17);
title('State Estimation Error', 'interpreter','latex', 'fontsize',17);
% hold off;

%% [n,m]=[4,4]
%% Phi values
Phi_1=tf([0 -1 0 0 0],[1 3.4 15 15 12]);
Phi_2=tf([0 0 -1 0 0],[1 3.4 15 15 12]);
Phi_3=tf([0 0 0 -1 0],[1 3.4 15 15 12]);
Phi_4=tf([0 0 0 0 -1],[1 3.4 15 15 12]);
Phi_5=tf([0 1 0 0 0],[1 3.4 15 15 12]);
Phi_6=tf([0 0 1 0 0],[1 3.4 15 15 12]);
Phi_7=tf([0 0 0 1 0],[1 3.4 15 15 12]);
Phi_8=tf([0 0 0 0 1],[1 3.4 15 15 12]);
[Phi_rs_1,Phi_1_t,]=lsim(Phi_1,y(:,1),t);
[Phi_rs_2,Phi_2_t,]=lsim(Phi_2,y(:,1),t);
[Phi_rs_3,Phi_3_t,]=lsim(Phi_3,y(:,1),t);
[Phi_rs_4,Phi_4_t,]=lsim(Phi_4,y(:,1),t);
[Phi_rs_5,Phi_5_t,]=lsim(Phi_5,u(:,1),t);
[Phi_rs_6,Phi_6_t,]=lsim(Phi_6,u(:,1),t);
[Phi_rs_7,Phi_7_t,]=lsim(Phi_7,u(:,1),t);
[Phi_rs_8,Phi_8_t,]=lsim(Phi_8,u(:,1),t);
Phi_rs=[Phi_rs_1, Phi_rs_2, Phi_rs_3,Phi_rs_4,Phi_rs_5,Phi_rs_6,Phi_rs_7,Phi_rs_8]; %Phi matrix

%% Estimated Parameters
theta_0=y(:,1)'*Phi_rs*inv(Phi_rs'*Phi_rs);
a_hat=zeros(4,1);
b_hat=zeros(4,1);
a_hat(1,1) = theta_0(1)+3.4;
a_hat(2,1) = theta_0(2)+15;
a_hat(3,1) = theta_0(3)+15;
a_hat(4,1) = theta_0(4)+12;
b_hat(1,1) = theta_0(5);
b_hat(2,1) = theta_0(6);
b_hat(3,1) = theta_0(7);
b_hat(4,1) = theta_0(8);

fprintf(['Estimated Parameters 4th Der.[Y&U] - Offline: a1_hat = ',num2str(a_hat(1,1)), ', a2_hat = ',num2str(a_hat(2,1)), ...
    ', a3_hat = ',num2str(a_hat(3,1)),', a4_hat = ',num2str(a_hat(4,1)), ...
    ', b1_hat = ',num2str(b_hat(1,1)), ', b2_hat = ',num2str(b_hat(2,1)), ...
    ', b3_hat = ',num2str(b_hat(3,1)), ', b4_hat = ',num2str(b_hat(4,1)), '\n'])

%% Estimated model
y_0=[0,0,0,0];
options=odeset('RelTol',10^(-10),'AbsTol',10^(-11));
[t_ode,y_ode] = ode45(@(t,x) ode_prosomoiwsh_9046_project_four_four(t,x,a_hat,b_hat),t,y_0,options);
y_error=y(:,1)-y_ode(:,1);

%% Evaluation Criteria
N=length(y);
n=length(theta_0);
I=0;
for i=1:length(y)
    I=I+y_error(i)^2;
end
I=I/N;

r=2;
Akaike_Criteria=N*log(I)+r*n;
Bayes_Criteria=N*log(I)+log(N)*n;
Khinchin_Law_Criteria=N*log(I)+2*r*log(log(N))*n;
FPE_Criteria=N*log(I)+N*log((N+n)/(N-n));
r1=2;
r2=3;
if(((n*log(2*N)-log(n)+r2)/N)>0)
    SRM_Criteria=I/(r1*sqrt((n*log(2*N)-log(n)+r2)/N));
else
    fprintf(['Passed the model maximum complexity\n']);
end

fprintf(['Evaluation Criteria: Akaike = ',num2str(Akaike_Criteria),...
    ', Bayes = ',num2str(Bayes_Criteria), ...
    ', Khinchins Law = ',num2str(Khinchin_Law_Criteria),...
    ', FPE = ',num2str(FPE_Criteria), ...
    ', SRM = ',num2str(SRM_Criteria), '\n\n'])

%% Plots
figure();
subplot(2,1,1);
hold on;
plot(t,y)
plot(t_ode,y_ode(:,1))
legend({['$y$'],['$\hat{y}$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y$'], 'interpreter','latex', 'fontsize',17);
title('State 4th Der.[Y/U] - Offline', 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,1,2);
hold on;
plot(t,y_error)
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y_{error}$'], 'interpreter','latex', 'fontsize',17);
title('State Estimation Error', 'interpreter','latex', 'fontsize',17);
hold off;

%% [n,m]=[4,3]
%% Phi values
Phi_1=tf([0 -1 0 0 0],[1 3.4 15 15 12]);
Phi_2=tf([0 0 -1 0 0],[1 3.4 15 15 12]);
Phi_3=tf([0 0 0 -1 0],[1 3.4 15 15 12]);
Phi_4=tf([0 0 0 0 -1],[1 3.4 15 15 12]);
Phi_5=tf([0 1 0 0],[1 3.4 15 15 12]);
Phi_6=tf([0 0 1 0],[1 3.4 15 15 12]);
Phi_7=tf([0 0 0 1],[1 3.4 15 15 12]);
[Phi_rs_1,Phi_1_t,]=lsim(Phi_1,y(:,1),t);
[Phi_rs_2,Phi_2_t,]=lsim(Phi_2,y(:,1),t);
[Phi_rs_3,Phi_3_t,]=lsim(Phi_3,y(:,1),t);
[Phi_rs_4,Phi_4_t,]=lsim(Phi_4,y(:,1),t);
[Phi_rs_5,Phi_5_t,]=lsim(Phi_5,u(:,1),t);
[Phi_rs_6,Phi_6_t,]=lsim(Phi_6,u(:,1),t);
[Phi_rs_7,Phi_7_t,]=lsim(Phi_7,u(:,1),t);
Phi_rs=[Phi_rs_1, Phi_rs_2, Phi_rs_3,Phi_rs_4,Phi_rs_5,Phi_rs_6,Phi_rs_7]; %Phi matrix

%% Estimated Parameters
theta_0=y(:,1)'*Phi_rs*inv(Phi_rs'*Phi_rs);
a_hat=zeros(4,1);
b_hat=zeros(2,1);
a_hat(1,1) = theta_0(1)+3.4;
a_hat(2,1) = theta_0(2)+15;
a_hat(3,1) = theta_0(3)+15;
a_hat(4,1) = theta_0(4)+12;
b_hat(1,1) = theta_0(5);
b_hat(2,1) = theta_0(6);
b_hat(3,1) = theta_0(7);

fprintf(['Estimated Parameters 4th Der.[Y] 3rd Der. [U] - Offline: a1_hat = ',num2str(a_hat(1,1)), ', a2_hat = ',num2str(a_hat(2,1)), ...
    ', a3_hat = ',num2str(a_hat(3,1)),', a4_hat = ',num2str(a_hat(4,1)), ...
    ', b1_hat = ',num2str(b_hat(1,1)), ', b2_hat = ',num2str(b_hat(2,1)), ...
    ', b3_hat = ',num2str(b_hat(3,1)), '\n'])

%% Estimated model
y_0=[0,0,0,0];
options=odeset('RelTol',10^(-10),'AbsTol',10^(-11));
[t_ode,y_ode] = ode45(@(t,x) ode_prosomoiwsh_9046_project_four_three(t,x,a_hat,b_hat),t,y_0,options);
y_error=y(:,1)-y_ode(:,1);

%% Evaluation Criteria
N=length(y);
n=length(theta_0);
I=0;
for i=1:length(y)
    I=I+y_error(i)^2;
end
I=I/N;

r=2;
Akaike_Criteria=N*log(I)+r*n;
Bayes_Criteria=N*log(I)+log(N)*n;
Khinchin_Law_Criteria=N*log(I)+2*r*log(log(N))*n;
FPE_Criteria=N*log(I)+N*log((N+n)/(N-n));
r1=2;
r2=3;
if(((n*log(2*N)-log(n)+r2)/N)>0)
    SRM_Criteria=I/(r1*sqrt((n*log(2*N)-log(n)+r2)/N));
else
    fprintf(['Passed the model maximum complexity\n']);
end

fprintf(['Evaluation Criteria: Akaike = ',num2str(Akaike_Criteria),...
    ', Bayes = ',num2str(Bayes_Criteria), ...
    ', Khinchins Law = ',num2str(Khinchin_Law_Criteria),...
    ', FPE = ',num2str(FPE_Criteria), ...
    ', SRM = ',num2str(SRM_Criteria), '\n\n'])

%% Plots
figure();
subplot(2,1,1);
hold on;
plot(t,y)
plot(t_ode,y_ode(:,1))
legend({['$y$'],['$\hat{y}$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y$'], 'interpreter','latex', 'fontsize',17);
title('State 4th Der.[Y] 3 Der. [U] - Offline', 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,1,2);
hold on;
plot(t,y_error)
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y_{error}$'], 'interpreter','latex', 'fontsize',17);
title('State Estimation Error', 'interpreter','latex', 'fontsize',17);
hold off;

%% Real-Time
phi=zeros(1,2);
theta_hat=zeros(1,2);
a_hat_rt = zeros(length(t),1);
b_hat_rt = zeros(length(t),1);
y_rt=zeros(length(y),4);
y_hat_rt=zeros(length(y),1);

%% Initializations
a_m=1;
gamma=1;
phi(1,1)=y(1,1)*exp((-a_m*t(1,1)));
phi(1,2)=u(1,1)*exp((-a_m*t(1,1)));
a_hat_rt(1)=0;
b_hat_rt(1)=0;
theta_hat(1,1)=a_m-a_hat_rt(1);
theta_hat(1,2)=b_hat_rt(1);

y_rt(1,:)=[phi(1,1) phi(1,2) theta_hat(1,1) theta_hat(1,2)];
% options=odeset('RelTol',10^(-10),'AbsTol',10^(-11));
% [t_rt,x_rt] = ode45(@(t,x) ode_prosomoiwsh_9046_project_rt(t,x,a_m,gamma,y),t,x_0_rt,options);
%% System
% Since we get our real state discretely, there is no reason for us to use
% continous time modeling. As ode45 cannot get realistic values between each
% discrete timestep
for i=1:length(y_rt)
    y_hat_rt(i,:)=[y_rt(i,3) y_rt(i,4)]*[y_rt(i,1) y_rt(i,2)]';
    
    e=y(i,1)-y_hat_rt(i,:);
    
    y_rt(i+1,1)=y_rt(i,1)-a_m*y_rt(i,1)+y(i,1);
    y_rt(i+1,2)=y_rt(i,2)-a_m*y_rt(i,2)+u(i,1);
    
    y_rt(i+1,3)=y_rt(i,3)+gamma*e*y_rt(i,1);
    y_rt(i+1,4)=y_rt(i,4)+gamma*e*y_rt(i,2);
end

y_err = y-y_hat_rt;
a_hat_rt=a_m-y_rt(1:length(y_rt)-1,3);
b_hat_rt=y_rt(1:length(y_rt)-1,4);

%% Evaluation Criteria
N=length(y);
n=2;
I=0;
for i=1:length(y)
    I=I+y_err(i)^2;
end
I=I/N;

r=2;
Akaike_Criteria=N*log(I)+r*n;
Bayes_Criteria=N*log(I)+log(N)*n;
Khinchin_Law_Criteria=N*log(I)+2*r*log(log(N))*n;
FPE_Criteria=N*log(I)+N*log((N+n)/(N-n));
r1=2;
r2=3;
if(((n*log(2*N)-log(n)+r2)/N)>0)
    SRM_Criteria=I/(r1*sqrt((n*log(2*N)-log(n)+r2)/N));
else
    fprintf(['Passed the model maximum complexity\n']);
end

fprintf(['Evaluation Criteria - Online: Akaike = ',num2str(Akaike_Criteria),...
    ', Bayes = ',num2str(Bayes_Criteria), ...
    ', Khinchins Law = ',num2str(Khinchin_Law_Criteria),...
    ', FPE = ',num2str(FPE_Criteria), ...
    ', SRM = ',num2str(SRM_Criteria), '\n'])

%% Plots
% Plots are designed for 1920x1080p
figure();
subplot(2,2,1);
hold on;
plot(t,y(:,1), 'LineWidth',2.0, 'Color','blue');
plot(t,y_hat_rt, 'LineWidth',2.0, 'Color','magenta');
legend({['$y$'],['$\hat{y}$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y$'], 'interpreter','latex', 'fontsize',17);
title('State - Online', 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,2,2);
plot(t,y_err, 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y_{error}$'], 'interpreter','latex', 'fontsize',14);
title('State estimation error', 'interpreter','latex', 'fontsize',11);

subplot(2,2,3);
plot(t,a_hat_rt, 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{a}$'], 'interpreter','latex', 'fontsize',14);
title('a estimation', 'interpreter','latex', 'fontsize',11);

subplot(2,2,4);
plot(t,b_hat_rt, 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{b}$'], 'interpreter','latex', 'fontsize',14);
title('b estimation', 'interpreter','latex', 'fontsize',11);


%% Real-Time
phi=zeros(1,2);
theta_hat=zeros(1,2);
a_hat_rt = zeros(length(t),1);
b_hat_rt = zeros(length(t),1);
y_rt=zeros(length(y),4);
y_hat_rt=zeros(length(y),1);

%% Initializations
a_m=1;
gamma=1;
sigma=1e-4;
M=1;
phi(1,1)=y(1,1)*exp((-a_m*t(1,1)));
phi(1,2)=u(1,1)*exp((-a_m*t(1,1)));
a_hat_rt(1)=0;
b_hat_rt(1)=0;
theta_hat(1,1)=a_m-a_hat_rt(1);
theta_hat(1,2)=b_hat_rt(1);

y_rt(1,:)=[phi(1,1) phi(1,2) theta_hat(1,1) theta_hat(1,2)];
% options=odeset('RelTol',10^(-10),'AbsTol',10^(-11));
% [t_rt,x_rt] = ode45(@(t,x) ode_prosomoiwsh_9046_project_rt(t,x,a_m,gamma,y),t,x_0_rt,options);
%% System
% Since we get our real state discretely, there is no reason for us to use
% continous time modeling. As ode45 cannot get realistic values between each
% discrete timestep
for i=1:length(y_rt)
    y_hat_rt(i,:)=[y_rt(i,3) y_rt(i,4)]*[y_rt(i,1) y_rt(i,2)]';
    
    e=y(i,1)-y_hat_rt(i,:);
    
    y_rt(i+1,1)=y_rt(i,1)-a_m*y_rt(i,1)+y(i,1);
    y_rt(i+1,2)=y_rt(i,2)-a_m*y_rt(i,2)+u(i,1);
    
    if abs(y_rt(i,3))>=2*M
        y_rt(i+1,3)=y_rt(i,3)+gamma*e*y_rt(i,1)-gamma*sigma*y_rt(i,3);
    elseif abs(y_rt(i,3))>=M
        y_rt(i+1,3)=y_rt(i,3)+gamma*e*y_rt(i,1)-gamma*(abs(y_rt(i,3))-M)*sigma*y_rt(i,3)/M;
    else
        y_rt(i+1,3)=y_rt(i,3)+gamma*e*y_rt(i,1);
    end
    
    if abs(y_rt(i,4))>=2*M
        y_rt(i+1,4)=y_rt(i,4)+gamma*e*y_rt(i,2)-gamma*sigma*y_rt(i,4);
    elseif abs(y_rt(i,4))>=M
        y_rt(i+1,4)=y_rt(i,4)+gamma*e*y_rt(i,2)-gamma*(abs(y_rt(i,4))-M)*sigma*y_rt(i,4)/M;
    else
        y_rt(i+1,4)=y_rt(i,4)+gamma*e*y_rt(i,2);
    end
end

y_err = y-y_hat_rt;
a_hat_rt=a_m-y_rt(1:length(y_rt)-1,3);
b_hat_rt=y_rt(1:length(y_rt)-1,4);

%% Evaluation Criteria
N=length(y);
n=2;
I=0;
for i=1:length(y)
    I=I+y_err(i)^2;
end
I=I/N;

r=2;
Akaike_Criteria=N*log(I)+r*n;
Bayes_Criteria=N*log(I)+log(N)*n;
Khinchin_Law_Criteria=N*log(I)+2*r*log(log(N))*n;
FPE_Criteria=N*log(I)+N*log((N+n)/(N-n));
r1=2;
r2=3;
if(((n*log(2*N)-log(n)+r2)/N)>0)
    SRM_Criteria=I/(r1*sqrt((n*log(2*N)-log(n)+r2)/N));
else
    fprintf(['Passed the model maximum complexity\n']);
end

fprintf(['Evaluation Criteria[sigma] - Online: Akaike = ',num2str(Akaike_Criteria),...
    ', Bayes = ',num2str(Bayes_Criteria), ...
    ', Khinchins Law = ',num2str(Khinchin_Law_Criteria),...
    ', FPE = ',num2str(FPE_Criteria), ...
    ', SRM = ',num2str(SRM_Criteria), '\n'])

%% Plots
% Plots are designed for 1920x1080p
figure();
subplot(2,2,1);
hold on;
plot(t,y(:,1), 'LineWidth',2.0, 'Color','blue');
plot(t,y_hat_rt, 'LineWidth',2.0, 'Color','magenta');
legend({['$y$'],['$\hat{y}$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y$'], 'interpreter','latex', 'fontsize',17);
title('State[sigma] - Online', 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,2,2);
plot(t,y_err, 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$y_{error}$'], 'interpreter','latex', 'fontsize',14);
title('State estimation error', 'interpreter','latex', 'fontsize',11);

subplot(2,2,3);
plot(t,a_hat_rt, 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{a}$'], 'interpreter','latex', 'fontsize',14);
title('a estimation', 'interpreter','latex', 'fontsize',11);

subplot(2,2,4);
plot(t,b_hat_rt, 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{b}$'], 'interpreter','latex', 'fontsize',14);
title('b estimation', 'interpreter','latex', 'fontsize',11);
