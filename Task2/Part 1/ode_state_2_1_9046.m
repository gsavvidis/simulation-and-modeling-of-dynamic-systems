%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
%% ode function based on sytem equations
function [dx] = ode_state_2_1_9046(t,x,a_m,gamma)
    u=5*sin(3*t);
    dx(1,1) = -2*x(1)+1*u;
    
    x_hat=[x(4) x(5)]*[x(2) x(3)]';

    e=x(1)-x_hat;
    
    dx(2,1)=-a_m*x(2)+x(1);
    dx(3,1)=-a_m*x(3)+u;
    
    dx(4,1)=gamma*e*x(2);
    dx(5,1)=gamma*e*x(3);
    
    if isnan(dx)
        return;
    end
end