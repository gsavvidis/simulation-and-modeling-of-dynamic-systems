%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
%% ode function based on sytem equations
function [dx] = ode_state_2_3_9046(t,x,A,B)
% State consists of [x_error(1:2), A_hat(3:6), B_hat(7:8), x(9:10)]
    u=10*sin(2*t)+5*sin(7.5*t);
    
    e_1=x(9)-x(1);
    e_2=x(10)-x(2);
    dx(9,1)=A(1,1)*x(9)+A(1,2)*x(10)+B(1,1)*u;
    dx(10,1)=A(2,1)*x(9)+A(2,2)*x(10)+B(2,1)*u;
    dx(1,1)=x(3)*x(1)+x(4)*x(2)+x(7)*u;
    dx(2,1)=x(5)*x(1)+x(6)*x(2)+x(8)*u;
    dx(3,1)=e_1*x(1);
    dx(4,1)=e_1*x(2);
    dx(5,1)=e_2*x(1);
    dx(6,1)=e_2*x(2);
    dx(7,1)=e_1*u;
    dx(8,1)=e_2*u;
    
    if isnan(dx)
        return;
    end
end