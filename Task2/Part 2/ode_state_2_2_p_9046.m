%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
%% ode function based on sytem equations
function [dx] = ode_state_2_2_p_9046(t,x,a,b)
    n=0.15*sin(2*pi*20*t);
    u=5*sin(3*t);
    a_tilde=x(2)-a;
    b_tilde=x(3)-b;

    dx(1,1) = -a*x(1)+a_tilde*((x(4)+n)-x(1))-b_tilde*u;
    dx(2,1)=-x(1)*((x(4)+n)-x(1));
    dx(3,1)=x(1)*u;
    dx(4,1)=-a*x(4)+b*u;
    
    if isnan(dx)
        return;
    end
end