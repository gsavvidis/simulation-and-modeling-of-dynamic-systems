%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
% a_11=-.25
% a_12=3
% a_21=-5
% a_22=-1
% b_1=1
% b_2=2.2
% e=x-x_hat
% A_tilde=A_hat-A
% B_tilde=B_hat-B
% u=10*sin(2*t)+5*sin(7.5*t);
%% Parallili Domi
% e_dot=A*e-A_tilde*x_hat-B_tilde*u
% x_dot=Ax+B*u
% x_hat_dot=A_hat*x_hat+B_hat*u
% A_dot_hat=x_hat*e'
% B_dot_hat=u*e'

clear all;
%% Matrix creation
t=0;
a_11=-.25;
a_12=3;
a_21=-5;
a_22=-1;
A=[a_11, a_12; a_21, a_22];
b_1=1;
b_2=2.2;
B=[b_1;b_2];
x=zeros(length(t),2);
x_hat=zeros(length(t),2);
A_hat = zeros(length(t),4);
B_hat = zeros(length(t),2);

%% Initializations
u=10*sin(2*t)+5*sin(7.5*t);
A_hat(1,:)=[0,0,0,0];
B_hat(1,:)=[0,0];

% State consists of [x_error(1:2), A_hat(3:6), B_hat(7:8), x(9:10)]
t_1=[0:0.00001:75];
y_0=[x_hat(1,:) A_hat(1,:) B_hat(1,:) x(1,:)];
options=odeset('RelTol',10^(-10),'AbsTol',10^(-11));
%% System Parallili Domi
[t,y] = ode45(@(t,x) ode_state_2_3_9046(t,x,A,B),t_1,y_0,options);

error=[(y(:,9)-y(:,1)), (y(:,10)-y(:,2))];
% error=[(y(:,1)-y(:,9)), (y(:,2)-y(:,10))];

%% Plots Mikti Domi
% Plots are designed for 1920x1080p
figure('Position',[50 40 900 960]);
subplot(2,2,1);
hold on;
plot(t,y(:,9), 'LineWidth',2.0, 'Color','blue');
plot(t,y(:,1), 'LineWidth',2.0, 'Color','magenta');
legend({['$x$'],['$\hat{x}$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$x$'], 'interpreter','latex', 'fontsize',17);
title('State', 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,2,2);
plot(t,error(:,1), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$x_{error}$'], 'interpreter','latex', 'fontsize',14);
title('State estimation error', 'interpreter','latex', 'fontsize',11);

subplot(2,2,3);
hold on;
plot(t,y(:,10), 'LineWidth',2.0, 'Color','blue');
plot(t,y(:,2), 'LineWidth',2.0, 'Color','magenta');
legend({['$x$'],['$\hat{x}$']}, 'interpreter','latex', 'fontsize',15);
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$x$'], 'interpreter','latex', 'fontsize',17);
title('State', 'interpreter','latex', 'fontsize',17);
hold off;

subplot(2,2,4);
plot(t,error(:,2), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$x_{error}$'], 'interpreter','latex', 'fontsize',14);
title('State estimation error', 'interpreter','latex', 'fontsize',11);

figure('Position',[1000 40 900 960]);
subplot(2,3,1);
plot(t,y(:,3), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{a}_{11}$'], 'interpreter','latex', 'fontsize',14);

subplot(2,3,2);
plot(t,y(:,4), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{a}_{12}$'], 'interpreter','latex', 'fontsize',14);

subplot(2,3,3);
plot(t,y(:,5), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{a}_{21}$'], 'interpreter','latex', 'fontsize',14);

subplot(2,3,4);
plot(t,y(:,6), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{a}_{22}$'], 'interpreter','latex', 'fontsize',14);

subplot(2,3,5);
plot(t,y(:,7), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{b}_{1}$'], 'interpreter','latex', 'fontsize',14);

subplot(2,3,6);
plot(t,y(:,8), 'LineWidth',2.0, 'Color','blue');  % Error for the first state
xlabel('Time [$s$]', 'interpreter','latex', 'fontsize',12);
ylabel(['$\hat{b}_{2}$'], 'interpreter','latex', 'fontsize',14);
