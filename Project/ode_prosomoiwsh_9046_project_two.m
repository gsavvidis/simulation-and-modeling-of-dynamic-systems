%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
%% ode function based on estimated system equations
function [dy] = ode_prosomoiwsh_9046_project_two(t,y,a,b)
    u=sin(t)+0.1*cos(2*t);
    u_dot=cos(t)-0.2*sin(2*t);
    dy=zeros(2,1);
    dy(1)=y(2);
    dy(2)=-a(1,1)*y(2)-a(2,1)*y(1)+b(1,1)*u_dot+b(2,1)*u;
    if isnan(dy)
        return;
    end
end