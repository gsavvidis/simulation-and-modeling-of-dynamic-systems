%% Georgios Savvidis AEM:9046 gsavvidis@ece.auth.gr
%% ode function based on estimated system equations
function [dy] = ode_prosomoiwsh_9046_project_three(t,y,a,b)
    u=sin(t)+0.1*cos(2*t);
    u_dot=cos(t)-0.2*sin(2*t);
    u_ddot=-sin(t)-0.4*cos(2*t);
%     u_dddot=-cos(t)+0.8*sin(2*t);
    dy=zeros(4,1);
    dy(1)=y(2);
    dy(2)=y(3);
    dy(3)=y(4);
    dy(4)=-a(1,1)*y(4)-a(2,1)*y(3)-a(3,1)*y(2)-a(4,1)*y(1)+b(1,1)*u_ddot+b(2,1)*u_dot+b(3,1)*u;
    if isnan(dy)
        return;
    end
end